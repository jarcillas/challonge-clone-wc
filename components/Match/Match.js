import { LitElement, html, css } from 'lit';

import '../Player/Player.js';

export class Match extends LitElement {
  constructor() {
    super();
    this.upPlayer = { name: null, win: false, glow: false };
    this.downPlayer = { name: null, win: false, glow: false };
    this.roundsData = null;
    this.roundNum = 1;
    this.matchNum = 1;
  }

  static properties = {
    upPlayer: { attribute: false },
    downPlayer: { attribute: false },
    roundsData: { attribute: false },
    roundNum: { attribute: false },
    matchNum: { attribute: false },
  };

  static styles = css`
    :host {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      height: 80px;
      margin: 10px 20px;
    }
  `;

  render() {
    return html`
      <player-elem
        .name=${this.upPlayer.name ?? null}
        .glow=${this.upPlayer.glow}
        .win=${this.upPlayer.win}
        .isUpPlayer=${true}
        .roundsData=${this.roundsData}
        .roundNum=${this.roundNum}
        .matchNum=${this.matchNum}
        .playerNum=${this.matchNum * 2 - 1}
      ></player-elem>
      <player-elem
        .name=${this.downPlayer.name ?? null}
        .glow=${this.downPlayer.glow}
        .win=${this.downPlayer.win}
        .isUpPlayer=${false}
        .roundsData=${this.roundsData}
        .roundNum=${this.roundNum}
        .matchNum=${this.matchNum}
        .playerNum=${this.matchNum * 2}
      ></player-elem>
    `;
  }
}

customElements.define('match-elem', Match);

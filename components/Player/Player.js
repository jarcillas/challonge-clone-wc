import { LitElement, html, css } from 'lit';
import { classMap } from 'lit/directives/class-map.js';

export class Player extends LitElement {
  constructor() {
    super();
    this.name = null;
    this.isUpPlayer = true;
    this.roundsData = null;
    this.roundData = null;
    this.roundNum = 1;
    this.matchNum = 1;
    this.playerNum = 1;
    this.glow = false;
    this.win = false;
    this.addEventListener('click', this._onPlayerClick);
    this.addEventListener('mouseenter', this._onPlayerMouseEnter);
    this.addEventListener('mouseleave', this._onPlayerMouseLeave);
  }

  static properties = {
    name: { attribute: false },
    isUpPlayer: { attribute: false },
    roundsData: { attribute: false },
    roundNum: { attribute: false },
    matchNum: { attribute: false },
    playerNum: { attribute: false },
    glow: { attribute: false },
    win: { attribute: false },
  };

  static styles = css`
    .player {
      height: 40px;
      width: 300px;
      display: flex;
      align-items: center;
      justify-content: center;
      font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    }

    .up {
      border-radius: 10px 10px 0 0;
      background-color: #bfdbf7;
      border-bottom: 1px solid #1f7a8c;
    }

    .down {
      border-radius: 0 0 10px 10px;
      background-color: #bfdbf7;
      border-top: 1px solid #1f7a8c;
    }

    .glow {
      background-color: #f1e469 !important;
    }

    .win,
    .winlastround {
      font-weight: bold;
      position: relative;
    }

    .win::after {
      content: '✔️';
      position: absolute;
      right: 10px;
    }

    .winlastround::after {
      content: '🏆';
      position: absolute;
      right: 10px;
    }
  `;

  _onPlayerClick() {
    if (this.name === 'Bye' || this.name === null) {
      return;
    }

    // creates a copy of roundsData which is a nested array of objects
    let newRoundsData = this.roundsData.map((roundArray) => {
      return roundArray.map((playerObj) => {
        return { ...playerObj };
      });
    });

    // updates the current Player component to have glow and win
    newRoundsData[this.roundNum - 1][this.playerNum - 1] = {
      name: this.name,
      glow: true,
      win: true,
    };
    // updates the Player component in the same match to have win = false
    if (this.isUpPlayer) {
      newRoundsData[this.roundNum - 1][this.playerNum].win = false;
    } else {
      newRoundsData[this.roundNum - 1][this.playerNum - 2].win = false;
    }

    if (this.roundNum < this.roundsData.length) {
      newRoundsData[this.roundNum][this.matchNum - 1] = {
        name: this.name,
        glow: true,
        win: false,
      };
      newRoundsData = this.clearRest(
        newRoundsData,
        this.roundNum + 2,
        Math.ceil(this.matchNum / 2)
      );
    }

    const options = {
      detail: { newRoundsData: newRoundsData },
      bubbles: true,
      composed: true,
    };

    this.dispatchEvent(new CustomEvent('data-changed', options));
  }

  clearRest(prevRoundsData, clearRoundNum, clearPlayerNum) {
    if (clearRoundNum > this.roundsData.length) {
      return prevRoundsData;
    } else {
      let nextRoundsData = prevRoundsData.map((roundArray) => {
        return roundArray.map((playerObj) => {
          return { ...playerObj };
        });
      });
      nextRoundsData[clearRoundNum - 1][clearPlayerNum - 1].name = null;
      nextRoundsData[clearRoundNum - 1][clearPlayerNum - 1].win = false;
      return this.clearRest(
        nextRoundsData,
        clearRoundNum + 1,
        Math.ceil(clearPlayerNum / 2)
      );
    }
  }

  _onPlayerMouseEnter() {
    if (this.name === 'Bye' || this.name === null) {
      return;
    }
    let newRoundsData = this.roundsData.map((roundData) => {
      if (roundData < this.roundNum) {
        return roundData;
      } else {
        return roundData.map((player) => {
          if (player.name === this.name) {
            return {
              ...player,
              glow: true,
            };
          } else {
            return {
              ...player,
              glow: false,
            };
          }
        });
      }
    });

    const options = {
      detail: { newRoundsData: newRoundsData },
      bubbles: true,
      composed: true,
    };

    this.dispatchEvent(new CustomEvent('data-changed', options));
  }

  _onPlayerMouseLeave() {
    let newRoundsData = this.roundsData.map((roundData) => {
      return roundData.map((player) => {
        return {
          ...player,
          glow: false,
        };
      });
    });

    const options = {
      detail: { newRoundsData: newRoundsData },
      bubbles: true,
      composed: true,
    };

    this.dispatchEvent(new CustomEvent('data-changed', options));
  }

  render() {
    const classInfo = {
      player: true,
      up: this.isUpPlayer,
      down: !this.isUpPlayer,
      glow: this.glow,
      win: this.win && this.roundNum < this.roundsData.length,
      winlastround: this.win && this.roundNum === this.roundsData.length,
    };

    return html` <div class=${classMap(classInfo)}>${this.name}</div>`;
  }
}

customElements.define('player-elem', Player);

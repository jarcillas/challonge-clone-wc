import { LitElement, html } from 'lit';

import './Brackets/Brackets.js';
import './NamesInput/NamesInput.js';

export class App extends LitElement {
  static properties = {
    roundsData: { attribute: false },
    showNamesInput: { attribute: false },
  };

  constructor() {
    super();

    this.roundsData = [];
    this.showNamesInput = true;

    this.addEventListener('data-changed', this._setRoundsData);
    this.addEventListener('toggle-input', (e) => {
      this.showNamesInput = e.detail.showInput;
    });
  }

  _setRoundsData(e) {
    this.roundsData = e.detail.newRoundsData;
    console.log(this.roundsData);
  }

  render() {
    return html`
      <names-input .showInput=${this.showNamesInput}></names-input>
      <brackets-elem
        .roundsData=${this.roundsData}
        .showInput=${this.showNamesInput}
      ></brackets-elem>
    `;
  }
}

customElements.define('app-elem', App);

import { LitElement, html, css } from 'lit';
import { repeat } from 'lit/directives/repeat.js';

import '../Round/Round.js';

export class Brackets extends LitElement {
  static properties = {
    roundsData: { attribute: false },
  };

  constructor() {
    super();
    this.roundsData = null;
  }

  static styles = css`
    :host {
      width: 100vw;
      min-height: 600px;
      display: flex;
    }
  `;

  render() {
    const renderedRounds = repeat(this.roundsData, (roundData, idx) => {
      return html`
        <round-elem
          .roundNum=${idx + 1}
          .roundsData=${this.roundsData}
        ></round-elem>
      `;
    });

    return html`${renderedRounds}`;
  }
}

customElements.define('brackets-elem', Brackets);

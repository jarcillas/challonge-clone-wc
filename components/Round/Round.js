import { LitElement, html, css } from 'lit';
import { repeat } from 'lit/directives/repeat.js';

import '../Match/Match.js';

export class Round extends LitElement {
  constructor() {
    super();
    this.roundNum = 1;
    this.roundsData = null;
  }

  static properties = {
    roundNum: { attribute: false },
    roundsData: { attribute: false },
  };

  static styles = css`
    :host {
      height: 100;
      flex-grow: 1;
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      padding-top: 20px;
      padding-bottom: 20px;
    }
  `;

  render() {
    const roundDataByMatch = this.roundsData[this.roundNum - 1].reduce(
      function (result, value, index, array) {
        if (index % 2 === 0) result.push(array.slice(index, index + 2));
        return result;
      },
      []
    );

    const renderedMatches = repeat(
      roundDataByMatch,
      ([upPlayer, downPlayer], idx) =>
        html`<match-elem
          .upPlayer=${upPlayer}
          .downPlayer=${downPlayer}
          .roundsData=${this.roundsData}
          .roundNum=${this.roundNum}
          .matchNum=${idx + 1}
        ></match-elem>`
    );

    return html`${renderedMatches}`;
  }
}

customElements.define('round-elem', Round);

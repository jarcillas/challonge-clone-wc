import {LitElement, html, css} from 'lit';

import {generateInitialRounds} from '../../helpers/index.js';

export class NamesInput extends LitElement {
  static properties = {
    showInput: {attribute: false},
    namesInput: {attribute: false},
  };

  static styles = css`
    #input-container {
      position: fixed;
      top: 0;
      right: 0;
      display: flex;
      flex-direction: column;
      padding: 10px;
      margin: 10px;
      width: 200px;
      z-index: 3;
    }

    #input-heading {
      font-size: 12px;
      font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
      color: #ffffff;
      margin-bottom: 2px;
    }

    #name-input {
      resize: none;
    }
  `;

  constructor() {
    super();

    // const defaultPlayers = [...Array(14).keys()].map((n) => `Player ${n + 1}`);

    const defaultPlayers = [
      'Jayson Tatum',
      'Stephen Curry',
      'Giannis Antetokounmpo',
      'Lebron James',
      'Kawhi Leonard',
      'Luka Doncic',
      'James Harden',
      'Nikola Jokic',
      'Damian Lillard',
      'Anthony Edwards',
      'Kevin Durant',
      'Ja Morant',
      'Demar Derozan',
      'Donovan Mitchell',
      'RJ Barrett',
      'Jimmy Butler',
    ];

    this.namesInput = defaultPlayers.join('\n');
  }

  _onNamesInputChange(e) {
    this.namesInput = e.target.value;
  }

  _onCreateBracketClick = (e) => {
    e.preventDefault();
    // eslint-disable-next-line no-undef
    const playerList = [...new Set(this.namesInput.split('\n'))]; // generate a list of players from the names input

    // Don't generate a bracket if there are less than 2 players
    if (playerList.length < 2) {
      return;
    }

    const options = {
      detail: {newRoundsData: generateInitialRounds(playerList)},
      bubbles: true,
      composed: true,
    };

    this.dispatchEvent(new CustomEvent('data-changed', options));
  };

  _onToggleButtonClick = (e) => {
    e.preventDefault();

    const options = {
      detail: {showInput: !this.showInput},
      bubbles: true,
      composed: true,
    };

    this.dispatchEvent(new CustomEvent('toggle-input', options));
  };

  render() {
    return html`
      <form id="input-container">
        <h4 ?hidden=${!this.showInput} id="input-heading">
          Please enter the names of the players/teams ordered by their seed:
        </h4>
        <textarea
          @input=${this._onNamesInputChange}
          ?hidden=${!this.showInput}
          id="name-input"
          cols="30"
          rows="20"
          .value=${this.namesInput}
          spellcheck=${false}
        ></textarea>
        <input
          ?hidden=${!this.showInput}
          type="submit"
          value="Create bracket"
          id="create-bracket-button"
          @click=${this._onCreateBracketClick}
        />
        <button @click=${this._onToggleButtonClick}>
          ${this.showInput ? 'Hide' : 'Show'}
        </button>
      </form>
    `;
  }
}

customElements.define('names-input', NamesInput);

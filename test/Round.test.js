import { Round } from '../components/Round/Round';
import { fixture, assert } from '@open-wc/testing';
import { html } from 'lit';

suite('round-elem', () => {
  test('is defined', () => {
    const el = document.createElement('round-elem');
    assert.instanceOf(el, Round);
  });

  const roundsData = [
    [
      { name: 'Jayson', win: false, glow: false },
      { name: 'Jaylen', win: false, glow: false },
      { name: 'Marcus', win: false, glow: false },
      { name: 'Robert', win: false, glow: false },
    ],
    [
      { name: null, win: false, glow: false },
      { name: null, win: false, glow: false },
    ],
  ];

  test('renders correctly with a given roundsData', async () => {
    const el = await fixture(
      html`<round-elem .roundsData=${roundsData}></round-elem>`
    );

    await el.updateComplete;
    assert.shadowDom.equal(
      el,
      `
      <match-elem></match-elem>
      <match-elem></match-elem>
      `
    );
    assert.equal(el.shadowRoot.children[0].upPlayer, roundsData[0][0]);
    assert.equal(el.shadowRoot.children[0].downPlayer, roundsData[0][1]);
    assert.deepEqual(el.shadowRoot.children[0].roundsData, roundsData);
    assert.equal(el.shadowRoot.children[0].roundNum, 1);
    assert.equal(el.shadowRoot.children[0].matchNum, 1);
    assert.equal(el.shadowRoot.children[1].upPlayer, roundsData[0][2]);
    assert.equal(el.shadowRoot.children[1].downPlayer, roundsData[0][3]);
    assert.deepEqual(el.shadowRoot.children[1].roundsData, roundsData);
    assert.equal(el.shadowRoot.children[1].roundNum, 1);
    assert.equal(el.shadowRoot.children[1].matchNum, 2);
  });

  test('renders styles correctly', async () => {
    const el = await fixture(
      html`<round-elem .roundsData=${roundsData}></round-elem>`
    );
    await el.updateComplete;
    assert.equal(getComputedStyle(el).height, '200px');
    assert.equal(getComputedStyle(el).flexGrow, '1');
    assert.equal(getComputedStyle(el).display, 'flex');
    assert.equal(getComputedStyle(el).flexDirection, 'column');
    assert.equal(getComputedStyle(el).justifyContent, 'space-around');
    assert.equal(getComputedStyle(el).paddingTop, '20px');
    assert.equal(getComputedStyle(el).paddingBottom, '20px');
  });
});

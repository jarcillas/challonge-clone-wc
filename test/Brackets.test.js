import { Brackets } from '../components/Brackets/Brackets';
import { fixture, assert } from '@open-wc/testing';
import { html } from 'lit';

suite('brackets-elem', () => {
  test('is defined', () => {
    const el = document.createElement('brackets-elem');
    assert.instanceOf(el, Brackets);
  });

  test('renders with a given roundsData value', async () => {
    const roundsData = [
      [
        { name: 'Jayson', win: false, glow: false },
        { name: 'Jaylen', win: false, glow: false },
        { name: 'Marcus', win: false, glow: false },
        { name: 'Robert', win: false, glow: false },
      ],
      [
        { name: null, win: false, glow: false },
        { name: null, win: false, glow: false },
      ],
    ];

    const el = await fixture(
      html`<brackets-elem .roundsData=${roundsData}></brackets-elem>`
    );
    assert.shadowDom.equal(
      el,
      `
      <round-elem></round-elem>
      <round-elem></round-elem>
      `
    );
  });
});

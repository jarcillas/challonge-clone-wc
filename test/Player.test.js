import { Player } from '../components/Player/Player.js';
import { fixture, assert, oneEvent } from '@open-wc/testing';
import { html } from 'lit';

suite('player-elem', () => {
  test('is defined', () => {
    const el = document.createElement('player-elem');
    assert.instanceOf(el, Player);
  });

  test('renders with default values', async () => {
    const el = await fixture(html`<player-elem></player-elem>`);
    assert.shadowDom.equal(
      el,
      `
      <div class="player up"></div>
      `
    );
  });

  test('renders with a set name', async () => {
    const el = await fixture(
      html`<player-elem .name=${'Jayson'}></player-elem>`
    );
    assert.shadowDom.equal(
      el,
      `
      <div class="player up">Jayson</div>
      `
    );
  });

  test('renders with correct classes if not an UpPlayer', async () => {
    const el = await fixture(
      html`<player-elem .isUpPlayer=${false}></player-elem>`
    );
    assert.shadowDom.equal(
      el,
      `
      <div class="player down"></div>
      `
    );
  });

  test('renders with correct classes if glow is true', async () => {
    const el = await fixture(html`<player-elem .glow=${true}></player-elem>`);
    assert.shadowDom.equal(
      el,
      `
      <div class="player up glow"></div>
      `
    );
  });

  test('renders with correct classes if win is true and roundNum < number of rounds', async () => {
    const roundsData = [1, 2];
    const el = await fixture(
      html`<player-elem
        .win=${true}
        .roundNum=${1}
        .roundsData=${roundsData}
      ></player-elem>`
    );
    assert.shadowDom.equal(
      el,
      `
      <div class="player up win"></div>
      `
    );
  });

  test('renders with correct classes if win is true and roundNum = number of rounds', async () => {
    const roundsData = [1, 2];
    const el = await fixture(
      html`<player-elem
        .win=${true}
        .roundNum=${2}
        .roundsData=${roundsData}
      ></player-elem>`
    );
    assert.shadowDom.equal(
      el,
      `
      <div class="player up winlastround"></div>
      `
    );
  });

  test('renders with correct styles for shadowdom elements', async () => {
    const el = await fixture(html`<player-elem></player-elem>`);
    await el.updateComplete;
    const divNode = el.shadowRoot.children[0];
    assert.equal(getComputedStyle(divNode).borderTopLeftRadius, '10px');
    assert.equal(getComputedStyle(divNode).borderTopRightRadius, '10px');
    assert.equal(
      getComputedStyle(divNode).backgroundColor,
      'rgb(191, 219, 247)'
    );
    assert.equal(
      getComputedStyle(divNode).borderBottomColor,
      'rgb(31, 122, 140)'
    );
    assert.equal(getComputedStyle(divNode).borderBottomWidth, '1px');
    assert.equal(getComputedStyle(divNode).borderBottomStyle, 'solid');
  });

  test('handles a click', async () => {
    const roundsData = [
      [
        { name: 'Jayson', win: false, glow: false },
        { name: 'Jaylen', win: false, glow: false },
        { name: 'Marcus', win: false, glow: false },
        { name: 'Robert', win: false, glow: false },
      ],
      [
        { name: null, win: false, glow: false },
        { name: null, win: false, glow: false },
      ],
    ];

    const expectedRoundsData = [
      [
        { name: 'Jayson', win: true, glow: true },
        { name: 'Jaylen', win: false, glow: false },
        { name: 'Marcus', win: false, glow: false },
        { name: 'Robert', win: false, glow: false },
      ],
      [
        { name: 'Jayson', win: false, glow: true },
        { name: null, win: false, glow: false },
      ],
    ];
    const el = await fixture(
      html`<player-elem
        .name=${'Jayson'}
        .roundsData=${roundsData}
        .roundData=${roundsData[0]}
      ></player-elem>`
    );
    const listener = oneEvent(el, 'data-changed');
    el.click();
    const { detail } = await listener;
    assert.deepEqual(detail.newRoundsData, expectedRoundsData);
  });
});

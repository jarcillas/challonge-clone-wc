import { Match } from '../components/Match/Match';
import { fixture, assert } from '@open-wc/testing';
import { html } from 'lit';

suite('match-elem', () => {
  test('is defined', () => {
    const el = document.createElement('match-elem');
    assert.instanceOf(el, Match);
  });

  test('renders with default values', async () => {
    const el = await fixture(html`<match-elem></match-elem>`);
    assert.shadowDom.equal(
      el,
      `
      <player-elem></player-elem>
      <player-elem></player-elem>
      `
    );
  });

  test('renders styles correctly', async () => {
    const el = await fixture(html`<match-elem></match-elem>`);
    await el.updateComplete;
    assert.equal(getComputedStyle(el).display, 'flex');
    assert.equal(getComputedStyle(el).flexDirection, 'column');
    assert.equal(getComputedStyle(el).alignItems, 'center');
    assert.equal(getComputedStyle(el).justifyContent, 'center');
    assert.equal(getComputedStyle(el).height, '80px');
    assert.equal(getComputedStyle(el).marginTop, '10px');
    assert.equal(getComputedStyle(el).marginBottom, '10px');
    assert.equal(getComputedStyle(el).marginLeft, '20px');
    assert.equal(getComputedStyle(el).marginRight, '20px');
  });
});
